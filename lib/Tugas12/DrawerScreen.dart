import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:finalsanberapp/Latihan/Authentication/LoginScreen.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  Future<Void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      print(auth.currentUser.email);
    }
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
              accountName: Text("Viandry P"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage("assets/img/Vian.png"),
              ),
              accountEmail: Text(auth.currentUser.email)),
          DrawerListTitle(
            iconData: Icons.group,
            title: "NewGroup",
            onTilePressed: () {},
          ),
          DrawerListTitle(
            iconData: Icons.lock,
            title: "New Secret Group",
            onTilePressed: () {},
          ),
          DrawerListTitle(
            iconData: Icons.notifications,
            title: "New Chanel Chat",
            onTilePressed: () {},
          ),
          DrawerListTitle(
            iconData: Icons.contacts,
            title: "contacts",
            onTilePressed: () {},
          ),
          DrawerListTitle(
            iconData: Icons.bookmark_border,
            title: "Saved Message",
            onTilePressed: () {},
          ),
          DrawerListTitle(
            iconData: Icons.phone,
            title: "Calls",
            onTilePressed: () {},
          ),
          DrawerListTitle(
            iconData: Icons.logout,
            title: "Logout",
            onTilePressed: () {
              _signOut().then((value) => Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => LoginScreen())));
            },
          ),
        ],
      ),
    );
  }
}

class DrawerListTitle extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTitle(
      {Key key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
