import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Account extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      print(auth.currentUser.email);
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Account'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset("assets/img/Vian.png",
                      height: 300, width: 400),
                ),
                SizedBox(height: 20),
                Center(child: Text('Viandry')),
                SizedBox(height: 20),
                Center(child: Text(auth.currentUser.email))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
