import 'package:flutter/material.dart';
import 'package:finalsanberapp/Tugas12/DrawerScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      print(auth.currentUser.email);
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Welcome " + auth.currentUser.email),
      ),
      drawer: DrawerScreen(),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.end,
                //   children: [
                //     IconButton(
                //         icon: Icon(Icons.notifications), onPressed: () {}),
                //     IconButton(
                //         icon: Icon(Icons.add_shopping_cart), onPressed: () {}),
                //   ],
                // ),
                Center(
                  child: Image.asset("assets/img/Groupdoctors.jpg",
                      height: 300, width: 400),
                ),
                // TextField(
                //   decoration: InputDecoration(
                //     prefixIcon: Icon(
                //       Icons.search,
                //       size: 18,
                //     ),
                //     border: OutlineInputBorder(
                //       borderRadius: BorderRadius.circular(10),
                //     ),
                //     hintText: 'Search',
                //   ),
                // ),
                // SizedBox(height: 30),
                Text(
                  'Recomended Place',
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                ),
                SizedBox(height: 10),
                SizedBox(
                  height: 300,
                  child: GridView.count(
                    padding: EdgeInsets.zero,
                    crossAxisCount: 2,
                    childAspectRatio: 1.491,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      for (var place in places)
                        Image.asset('assets/img/$place.png')
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

final places = ['Monas', 'Roma', 'Berlin', 'Tokyo'];
