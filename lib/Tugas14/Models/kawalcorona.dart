// To parse this JSON data, do
//
//     final kawalcorona = kawalcoronaFromJson(jsonString);

import 'dart:convert';

List<Kawalcorona> kawalcoronaFromJson(String str) => List<Kawalcorona>.from(
    json.decode(str).map((x) => Kawalcorona.fromJson(x)));

String kawalcoronaToJson(List<Kawalcorona> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Kawalcorona {
  Kawalcorona({
    this.attributes,
  });

  Attributes attributes;

  factory Kawalcorona.fromJson(Map<String, dynamic> json) => Kawalcorona(
        attributes: Attributes.fromJson(json["attributes"]),
      );

  Map<String, dynamic> toJson() => {
        "attributes": attributes.toJson(),
      };
}

class Attributes {
  Attributes({
    this.objectid,
    this.countryRegion,
    this.lastUpdate,
    this.lat,
    this.long,
    this.confirmed,
    this.deaths,
    this.recovered,
    this.active,
  });

  int objectid;
  String countryRegion;
  int lastUpdate;
  double lat;
  double long;
  int confirmed;
  int deaths;
  int recovered;
  int active;

  factory Attributes.fromJson(Map<String, dynamic> json) => Attributes(
        objectid: json["OBJECTID"],
        countryRegion: json["Country_Region"],
        lastUpdate: json["Last_Update"],
        lat: json["Lat"] == null ? null : json["Lat"].toDouble(),
        long: json["Long_"] == null ? null : json["Long_"].toDouble(),
        confirmed: json["Confirmed"],
        deaths: json["Deaths"],
        recovered: json["Recovered"] == null ? null : json["Recovered"],
        active: json["Active"] == null ? null : json["Active"],
      );

  Map<String, dynamic> toJson() => {
        "OBJECTID": objectid,
        "Country_Region": countryRegion,
        "Last_Update": lastUpdate,
        "Lat": lat == null ? null : lat,
        "Long_": long == null ? null : long,
        "Confirmed": confirmed,
        "Deaths": deaths,
        "Recovered": recovered == null ? null : recovered,
        "Active": active == null ? null : active,
      };
}
