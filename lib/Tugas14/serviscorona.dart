import 'package:finalsanberapp/Tugas14/Models/kawalcorona.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ServicesCorona {
  static Future<Attributes> getData() async {
    final response = await http.get(Uri.parse('https://api.kawalcorona.com/'));
    var result = json.decode(response.body);
    print(result);

    return Attributes.fromJson(json.decode(response.body).toList());
  }
}
