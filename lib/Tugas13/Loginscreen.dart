import 'package:flutter/material.dart';
import 'HomeScreen.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 60),
                Center(
                  child: Text(
                    'Sanber Flutter',
                    // textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Color.fromRGBO(84, 197, 248, 1),
                      fontSize: 30,
                    ),
                  ),
                ),
                SizedBox(height: 40),
                Center(
                    child: Image.asset('assets/img/flutter.png',
                        width: 100, height: 100)),
                SizedBox(height: 40),
                TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    hintText: 'Username',
                  ),
                ),
                SizedBox(height: 30),
                TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    hintText: 'Password',
                  ),
                ),
                SizedBox(height: 30),
                Center(
                  child: InkWell(
                    child: Text(
                      "Forgot Password",
                      style: TextStyle(
                        color: Color.fromRGBO(41, 182, 246, 1),
                        fontSize: 14,
                      ),
                    ),
                    onTap: () {
                      print("value of your text");
                    },
                  ),
                ),
                SizedBox(height: 20),
                Center(
                  child: ConstrainedBox(
                    constraints:
                        BoxConstraints.tightFor(width: 350, height: 45),
                    child: ElevatedButton(
                      child: Text('Login'),
                      onPressed: () {
                        Route route = MaterialPageRoute(
                            builder: (context) => HomeScreen());
                        Navigator.push(context, route);
                      },
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromRGBO(84, 197, 248, 1), // background
                        onPrimary: Colors.white, // foreground
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Does not have account ? ',
                      ),
                      InkWell(
                        child: Text(
                          " Sign In",
                          style: TextStyle(
                            color: Color.fromRGBO(41, 182, 246, 1),
                            fontSize: 14,
                          ),
                        ),
                        onTap: () {
                          print("value of your text");
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
